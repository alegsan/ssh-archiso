![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/alegsan/ssh-archiso/master.svg?style=for-the-badge)

# SSH Archlinux Live System Image

Archlinux Live System Image that has ssh enabled and permits empty passwords.

Image is build by a daily ci cron job on an specific docker runner.

The [arch-install](https://gitlab.com/schmidtandreas/arch-install) project is
using this image to test the arch installation based on config csv files.
