#!/bin/bash -x

# build an archlinux iso image with ssh enabled on startup.
# we permit empty passwords because we use this iso to test
# our arch-install script in an ci enabled environment.

ci_project_dir="$1"
relang_dir="/usr/share/archiso/configs/releng/"
iso_build_dir="/tmp/archiso"
pacman_archiso_pkg="archiso"
airootfs_path="airootfs/root/customize_airootfs.sh"

[ -z "$ci_project_dir" ] && exit 1

# /run/shm does not exist and the link is broken. To correct this error, create
# a directory /run/shm
# (see https://wiki.archlinux.org/index.php/Install_from_existing_Linux#Installation_tips)
mkdir /run/shm

# install missing packages/applications
pacman -S grep file --noconfirm

# install archiso package
pacman -S "$pacman_archiso_pkg" --noconfirm
[ ! -d "$relang_dir" ] && exit 1

# use relang to get the same image packages like a official archlinux
#  iso image.
cp -R "$relang_dir" "$iso_build_dir"

# enable ssh and permit empty passwords
echo "sed -i 's/#\(PermitEmptyPasswords \).\+/\1yes/' /etc/ssh/sshd_config" \
			>> $iso_build_dir/$airootfs_path

# start sshd after pacman-key init is done
echo "sed -i 's/After=network/After=pacman-init.service network/' /usr/lib/systemd/system/sshd.service" \
			>> $iso_build_dir/$airootfs_path

echo "systemctl enable sshd" >> $iso_build_dir/$airootfs_path

# boot directly to archiso without any boot prompt and timeout
sed -i '1d' $iso_build_dir/syslinux/archiso_sys.cfg
sed -i '1iPROMPT 0' $iso_build_dir/syslinux/archiso_sys.cfg
sed -i '2iTIMEOUT 20' $iso_build_dir/syslinux/archiso_sys.cfg
sed -i '3iDEFAULT arch64' $iso_build_dir/syslinux/archiso_sys.cfg
sed -i 's|^timeout.*|timeout 0|' $iso_build_dir/efiboot/loader/loader.conf
sed -i 's|^default.*|default archiso*|' $iso_build_dir/efiboot/loader/loader.conf

# now build the whole archlinux iso image
mkarchiso -v -w /tmp/archiso-tmp $iso_build_dir/
[ $? -ne 0 ] && exit 1

# copy iso image to ci project dir to make it available as an artifact
# (see .gitlab-ci.yml)
cp out/archlinux-*.iso $ci_project_dir
md5sum out/archlinux-*.iso > $ci_project_dir/md5.txt
